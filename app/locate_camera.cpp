
#include<iostream>
#include<algorithm>
#include<fstream>
#include<chrono>

#include<ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <tf/transform_broadcaster.h>

#include<opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <Localizer.h>



using namespace std;

std::string dataset_record_path ;
std::ofstream fisheye_ofs;


std::shared_ptr<ORB_SLAM2::Localizer> localizer;

class ImageGrabber
{
public:
    ImageGrabber(){}

    void GrabImage(const sensor_msgs::ImageConstPtr& msg);
    ros::Publisher* pPosPub;
};




int main(int argc, char **argv)
{

    if (argc != 4) {
        std::cerr<<"Usage: locate_camera localization_map camera_config_file voc "<<std::endl;
        return -1;
    }
    ros::init(argc, argv, "Locate");
    ros::start();

    ImageGrabber igb;
    ros::NodeHandle nodeHandler;


    std::string map = std::string(argv[1]);
    std::string configFile = std::string(argv[2]);
    std::string vocFile = std::string(argv[3]);

    localizer = std::make_shared<ORB_SLAM2::Localizer>(map, configFile, vocFile);

    ros::Subscriber sub = nodeHandler.subscribe("Camera0", 1, &ImageGrabber::GrabImage,&igb);
    // Pose broadcaster
    ros::spin();

    fisheye_ofs.close();
    return 0;
}

void ImageGrabber::GrabImage(const sensor_msgs::ImageConstPtr& msg)
{
    // Copy the ros image message to cv::Mat.
    cv_bridge::CvImageConstPtr cv_ptr;
    try
    {
        cv_ptr = cv_bridge::toCvShare(msg);
    }
    catch (cv_bridge::Exception& e)
    {
        ROS_ERROR("cv_bridge exception: %s", e.what());
        return;
    }



    Eigen::Isometry3d T_wc;
    bool success = localizer->LocateFrame(cv_ptr->image, 0, T_wc);
    ORB_SLAM2::LocateResult lr;
    std::vector<cv::KeyPoint> inlier_point2d;
    std::vector<cv::KeyPoint> inlier_reprojected;

    cv::Mat colorImage;
    cv::cvtColor(cv_ptr->image,colorImage,CV_GRAY2RGB);
    if (success) {
        lr  = localizer->getCurrentLocateResult();
        inlier_point2d = lr.inlier_point2Ds;
        inlier_reprojected = localizer->ProjectLandmarkToFrame();

       // ofs<< T_wc.translation().transpose() << std::endl;


        cv::Scalar red(0,0,255);
        cv::Scalar blue(255,0,0);
        cv::drawKeypoints(colorImage,inlier_point2d,colorImage,red);
        cv::drawKeypoints(colorImage,inlier_reprojected,colorImage,blue);

    }

    std::cout<<"Locate success : "<< success << " " <<lr.num_inlier<<std::endl;



    cv::imshow("image",colorImage);
    cv::waitKey(1);

}

