
#include<iostream>
#include<algorithm>
#include<fstream>
#include<chrono>
#include<mutex>

#include<ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>

#include<opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
using namespace std;

std::string dataset_record_path ;
std::ofstream fisheye_ofs;
std::ofstream pose_ofs;
std::mutex file_mutex;
std::shared_ptr<tf::TransformListener> listener;
class ImageGrabber
{
public:
    ImageGrabber(){}

    void GrabImage(const sensor_msgs::ImageConstPtr& msg);
    ros::Publisher* pPosPub;

};


void createDatasetDir(const std::string& dir) {
    std::string cmd_str_mk = "mkdir -p \"" + dir + "\"";
    bool sucess = system(cmd_str_mk.c_str());

    std::string cam0 = dir + "/cam0";
    cmd_str_mk = "mkdir -p \"" + cam0 + "\"";
    sucess = system(cmd_str_mk.c_str());

    std::string map = dir + "/map";
    cmd_str_mk = "mkdir -p \"" + map + "\"";
    sucess = system(cmd_str_mk.c_str());

    std::string fisheye = dir + "/fisheye.txt";
    std::string cmd_str_touch = "touch \"" + fisheye+ "\"";
    sucess = system(cmd_str_touch.c_str());

    std::string  pose = dir + "/pose.txt";
    cmd_str_touch = "touch \"" + pose + "\"";
    sucess = system(cmd_str_touch.c_str());

}

int main(int argc, char **argv)
{

    if (argc != 2) {
        std::cerr<<"Usage: record_dataset <data_setpath>"<<std::endl;
        return -1;
    }
    ros::init(argc, argv, "Mono");
    ros::start();

	ImageGrabber igb;
    ros::NodeHandle nodeHandler;


    listener = std::make_shared<tf::TransformListener>();

    dataset_record_path = std::string(argv[1]);
    std::string fisheye_list = dataset_record_path + "/fisheye.txt";
    std::string pose_list = dataset_record_path + "/pose.txt";


    //  mkdir and files
    createDatasetDir(dataset_record_path);


    file_mutex.lock();
    fisheye_ofs.open(fisheye_list);
    if (!fisheye_ofs.is_open()) {
        std::cout << "can not open fisheye list file"<<std::endl;
        return 0;
    }

    pose_ofs.open(pose_list);
    if (!pose_ofs.is_open()) {
        std::cout << "can not open pose list file"<<std::endl;
        return 0;
    }
    file_mutex.unlock();



    ros::Subscriber sub = nodeHandler.subscribe("Camera0", 1, &ImageGrabber::GrabImage,&igb);
    // Pose broadcaster
    ros::spin();


    file_mutex.lock();
    fisheye_ofs.close();
    pose_ofs.close();
    file_mutex.unlock();

    return 0;
}

void ImageGrabber::GrabImage(const sensor_msgs::ImageConstPtr& msg)
{
    // Copy the ros image message to cv::Mat.
    cv_bridge::CvImageConstPtr cv_ptr;
    try
    {
        cv_ptr = cv_bridge::toCvShare(msg);
    }
    catch (cv_bridge::Exception& e)
    {
        ROS_ERROR("cv_bridge exception: %s", e.what());
        return;
    }




    std::string filename = std::to_string(cv_ptr->header.stamp.toNSec()) + ".jpg";
    std::string full_filename = dataset_record_path + "/cam0/" + filename;

    tf::StampedTransform transform;
    try{
        listener->lookupTransform("map", "base_footprint",
                                  ros::Time(0), transform);
    }
    catch (tf::TransformException &ex) {
        ROS_ERROR("%s",ex.what());
        ros::Duration(0.10).sleep();
        return;
    }

    file_mutex.lock();
    fisheye_ofs << cv_ptr->header.stamp.toNSec() <<std::endl;
    pose_ofs << cv_ptr->header.stamp.toNSec()
             << " " << transform.getOrigin().x()
             << " " << transform.getOrigin().y()
             << " " << transform.getOrigin().z()
             << " " << transform.getRotation().w()
             << " " << transform.getRotation().x()
             << " " << transform.getRotation().y()
             << " " << transform.getRotation().z()<<std::endl;
    file_mutex.unlock();



    std::cout<<"diff: "<<  cv_ptr->header.stamp.toNSec() - transform.stamp_.toNSec()<<std::endl;
    std::cout<<" tf: "<<transform.stamp_.toNSec()<< " " << transform.getOrigin().x() << " " << transform.getOrigin().y()
             << " " << transform.getOrigin().z()<< " " << transform.getRotation().w()
                    << " " << transform.getRotation().x()
                    << " " << transform.getRotation().y()
                    << " " << transform.getRotation().z()<<std::endl;


    cv::imwrite( full_filename, cv_ptr->image );
    std::cout<< "saving image to: "<<full_filename<<std::endl;
    cv::imshow("image", cv_ptr->image);
    cv::waitKey(30);
}

