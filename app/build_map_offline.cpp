
#include<iostream>
#include<algorithm>
#include<fstream>
#include<chrono>

#include<opencv2/core/core.hpp>

#include<System.h>


using namespace std;



void LoadImages(const string &strImagePath, const string &strPathTimes,
                vector<string> &vstrImages, vector<int64_t> &vTimeStamps);
void LoadExtendPose(const string &strPoseFile,
                    std::vector<ORB_SLAM2::StampedPose>& poseList );

void AlignTrajectoryAndOutput(const std::vector<ORB_SLAM2::StampedPose>& t0,
                              const std::vector<ORB_SLAM2::StampedPose>& t1,
                              std::string& outputPoseFile,
                              std::string& outputTrajectoryFile) ;

std::mutex pose_mutex;
bool availible_pose = false;
Eigen::Isometry3d tracked_T_wc;

void poseCallback(const Eigen::Isometry3d& pose) {
    pose_mutex.lock();
    availible_pose = true;
    tracked_T_wc = pose;
    std::cout<<"pose: \n"<<pose.matrix() <<std::endl;
    pose_mutex.unlock();
}

bool getTrackedPose(Eigen::Isometry3d& pose ) {
    pose_mutex.lock();
    bool b = availible_pose;
    pose = tracked_T_wc;
    pose_mutex.unlock();
    return b;
}


int main(int argc, char **argv)
{
    if(argc != 4)
    {
        cerr << endl << "Usage: ./build_localization_map path_to_vocabulary "
                "path_to_dataset camera_config" << endl;
        return 1;
    }

    // Retrieve paths to images
    vector<string> vstrImageFilenames;
    vector<int64_t> vTimestamps;
    std::string dataset_path = std::string(argv[2]);
    std::string strPathTimes  = dataset_path + "/fisheye.txt";

    std::cout<<"Fisheye : "<< strPathTimes<<std::endl;
    std::cout<<"voc:"<< std::string(argv[1])<<std::endl;
    LoadImages(dataset_path, strPathTimes, vstrImageFilenames, vTimestamps);

    int nImages = vstrImageFilenames.size();

    if(nImages<=0)
    {
        cerr << "ERROR: Failed to load images" << endl;
        return 1;
    }


    std::string camera_config = std::string(argv[3]);
    // Create SLAM system. It initializes all system threads and gets ready to process frames.
    ORB_SLAM2::System SLAM(argv[1],camera_config,ORB_SLAM2::System::MONOCULAR,true);


    // Vector for tracking time statistics
    vector<float> vTimesTrack;
    vTimesTrack.resize(nImages);

    cout << endl << "-------" << endl;
    cout << "Start processing sequence ..." << endl;
    cout << "Images in the sequence: " << nImages << endl << endl;

    // Main loop
    cv::Mat im;
    for(int ni=0; ni<nImages; ni++)
    {
        // Read image from file
        im = cv::imread(vstrImageFilenames[ni],CV_LOAD_IMAGE_UNCHANGED);
        int64_t tframe = vTimestamps[ni];

        if(im.empty())
        {
            cerr << endl << "Failed to load image at: "
                 <<  vstrImageFilenames[ni] << endl;
            return 1;
        }

#ifdef COMPILEDWITHC11
        std::chrono::steady_clock::time_point t1 = std::chrono::steady_clock::now();
#else
        std::chrono::monotonic_clock::time_point t1 = std::chrono::monotonic_clock::now();
#endif

        std::cout<<"Processing image: "<<tframe<< " " << ni <<"/"<< nImages<<std::endl;

        // Pass the image to the SLAM system
        SLAM.TrackMonocular(im,tframe);



#ifdef COMPILEDWITHC11
        std::chrono::steady_clock::time_point t2 = std::chrono::steady_clock::now();
#else
        std::chrono::monotonic_clock::time_point t2 = std::chrono::monotonic_clock::now();
#endif

        double ttrack= std::chrono::duration_cast<std::chrono::duration<double> >(t2 - t1).count();

        vTimesTrack[ni]=ttrack;

        // Wait to load the next frame
        double T=0;
        if(ni<nImages-1)
            T = (vTimestamps[ni+1]-tframe)/1e9;
        else if(ni>0)
            T = (tframe-vTimestamps[ni-1])/1e9;

        if(ttrack<T)
            usleep((T-ttrack)*1e6);
    }

    // Stop all threads
    SLAM.Shutdown();

    // Tracking time statistics
    sort(vTimesTrack.begin(),vTimesTrack.end());
    float totaltime = 0;
    for(int ni=0; ni<nImages; ni++)
    {
        totaltime+=vTimesTrack[ni];
    }
    cout << "-------" << endl << endl;
    cout << "median tracking time: " << vTimesTrack[nImages/2] << endl;
    cout << "mean tracking time: " << totaltime/nImages << endl;

    // Save camera trajectory
    std::string trajectory_save = dataset_path + "/map/KeyFrameTrajectory.txt";
    SLAM.SaveKeyFrameTrajectoryTUM(trajectory_save);

    std::string binary_map_save = dataset_path + "/map/map.bin";
    SLAM.SaveMap(binary_map_save);


    // extract aligned trajectorys
    std::vector<ORB_SLAM2::StampedPose>
            keyframe_Pose = SLAM.GetKeyframeTrajectory();


//
    std::string poseFile = dataset_path + "/pose.txt";
    std::vector<ORB_SLAM2::StampedPose> poses;
    LoadExtendPose(poseFile, poses);
//
    std::string alignedPose
            = dataset_path + "/map/AlignedPose.txt";
    std::string alignedTrajectory
            = dataset_path + "/map/AlignedTrajectory.txt";
    AlignTrajectoryAndOutput(poses, keyframe_Pose, alignedPose, alignedTrajectory);
//

    return 0;
}

void LoadImages(const string &strImagePath, const string &strPathTimes,
                vector<string> &vstrImages, vector<int64_t> &vTimeStamps)
{
    ifstream fTimes;
    fTimes.open(strPathTimes.c_str());
    vTimeStamps.reserve(5000);
    vstrImages.reserve(5000);
    while(!fTimes.eof())
    {
        string s;
        getline(fTimes,s);
        if(!s.empty())
        {
            stringstream ss;
            ss << s;
            std::string file = strImagePath + "/cam0/" + ss.str() + ".jpg";
            vstrImages.push_back(file);
            int64_t t;
            ss >> t;
            vTimeStamps.push_back(t);
        }
    }
}

void LoadExtendPose(const string &strPoseFile,
               std::vector<ORB_SLAM2::StampedPose>& poseList )
{
    ifstream fTimes;
    fTimes.open(strPoseFile.c_str());
    string oneLine;

    poseList.clear();
    while(!fTimes.eof())
    {
        std::getline(fTimes, oneLine);
        std::stringstream stream(oneLine);
        std::string s;
        std::getline(stream, s, ' ');
        if (s.empty()) continue;
        //std::cout<< s <<std::endl;
        std::string nanoseconds = s.substr(
                s.size() - 9, 9);
        std::string seconds = s.substr(
                0, s.size() - 9);
        okvis::Time t = okvis::Time(std::stoi(seconds), std::stoi(nanoseconds));
        //std::cout<<"t: "<<t <<std::endl;


        Eigen::Vector3d t_WS;
        Eigen::Vector4d q_WS;

        for (int j = 0; j < 3; ++j) {
            std::getline(stream, s, ' ');
//            std::cout<<s <<" ";
            t_WS[j] = atof(s.c_str());
        }

        for (int j = 0; j < 4; ++j) {
            std::getline(stream, s, ' ');
//            std::cout<<s <<" ";
            q_WS[j] = atof(s.c_str());
        }

        ORB_SLAM2::StampedPose p;
        p.ts = t;
        p.t_WS = t_WS;
        p.q_WS = q_WS;
        poseList.push_back(p);


        std::cout<<t<< " " <<p.t_WS.transpose()
                                         << " "<< p.q_WS.transpose()<<std::endl;
    }

    std::cout <<"Load external pose : "<< poseList.size() << std::endl;
}



void AlignTrajectoryAndOutput(const std::vector<ORB_SLAM2::StampedPose>& t0,
                              const std::vector<ORB_SLAM2::StampedPose>& t1,
                                std::string& outputPoseFile,
                              std::string& outputTrajectoryFile) {
    std::ofstream pose_ofs;
    pose_ofs.open(outputPoseFile);
    if (!pose_ofs.is_open()) {
        std::cout<< "Can not open outputPoseFile : " << outputPoseFile <<std::endl;
        return ;

    }

    std::ofstream tra_ofs;
    tra_ofs.open(outputTrajectoryFile);
    if (!tra_ofs.is_open()) {
        std::cout<< "Can not open outputTrajectoryFile : "
                 << outputTrajectoryFile <<std::endl;
        return ;

    }


    int cnt0, cnt1;
    cnt0 = 0;
    cnt1 = 0;

    std::cout << "t0: "<< t0.at(0).ts<<std::endl;
    std::cout << "t1: "<< t1.at(0).ts<<std::endl;

    while (t1.at(cnt1).ts < t0.at(0).ts) cnt1 ++;

    while (t0.at(cnt0).ts < t1.at(cnt1).ts) cnt0 ++;
    cnt0 --;

    std::cout<<"cnt0 : " << cnt0<<std::endl;
    std::cout<<"cnt1 : " << cnt1<<std::endl;

    std::cout << "t0 start: "<< t0.at(cnt0).ts<<std::endl;
    std::cout << "t1 start: "<< t1.at(0).ts<<std::endl;

    for (int i  = cnt1 ; i < t1.size() ; i ++) {
        okvis::Time fiducial = t1.at(i).ts;

        std::cout<<"fiducial : "<<i << " "<< fiducial <<std::endl;
        while (std::abs(t0.at(cnt0).ts.toSec() - fiducial.toSec() ) > 0.005
               && cnt0 < t0.size() -1 ) {
            cnt0 ++;
        }

        std::cout<<"find : "<< cnt0 << " "<< t0.at(cnt0).ts <<std::endl;

        pose_ofs<< t1.at(i).ts << " " <<t0.at(cnt0).t_WS.transpose()
                                        <<std::endl;
        tra_ofs<< t1.at(i).ts << " " <<t1.at(i).t_WS.transpose()
                <<std::endl;

//        std::cout<< t1.at(i).ts << " " <<t0.at(cnt0).t_WS.transpose()
//                                         << " "<< t0.at(cnt0).q_WS.transpose()<<std::endl;
//        std::cout<< t1.at(i).ts << " " <<t1.at(i).t_WS.transpose()
//               << " "<< t1.at(i).q_WS.transpose() <<std::endl;
    }


    pose_ofs.close();
    tra_ofs.close();

}